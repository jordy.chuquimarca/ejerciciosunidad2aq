----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:13:26 07/11/2022 
-- Design Name: 
-- Module Name:    Eje18 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Eje18 is
    Generic(contador_valor: integer = 511);
    Port ( clk,lda,clr,en : in  STD_LOGIC;
           data : in  integer range 0 to contador_valor;
           Q : inout  integer range 0 to contador_valor);
end Eje18;

architecture Behavioral of Eje18 is
signal contador_signal: integer range 0 to contador_valor;
begin
process (clk,en,lda,clr)
begin
if(clr = '0')then
  contador_signal <= '1';
 elsif(clk' event and clk = '1') then
    if (en = '1') then
	   if (lda = '0') then
	     contador_signal <= data;
	 else
	     contador_signal <= data;
    end if;
   end if;
  end if;
 end process;
end Behavioral;

