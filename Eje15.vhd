----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:41:28 07/10/2022 
-- Design Name: 
-- Module Name:    Eje15 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Eje15 is
    Port ( clk, UP : in  STD_LOGIC;
           Q : inout  STD_LOGIC_VECTOR (3 downto 0));
end Eje15;

architecture Behavioral of Eje15 is

begin
process (UP, clk) begin
  if(clk' event and clk = '1') then 
    if (UP = '0') then
	     Q <= Q + 1;
	else 
	     Q <= q - 1;
	end if;
 end if;
end process;


end Behavioral;

