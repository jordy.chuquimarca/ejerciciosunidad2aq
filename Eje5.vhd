----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:18:39 07/10/2022 
-- Design Name: 
-- Module Name:    Eje5 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Eje5 is
    Port ( clk,RESET,EN,D : in  STD_LOGIC;
           Q : inout  STD_LOGIC);
end Eje5;

architecture Behavioral of Eje5 is
signal q_aux : std_logic;
begin 
process (clk,RESET,EN)
begin
if RESET = '1' then q_aux <= '0';
   elsif (clk' event and clk='1') then
	  if (EN = '1') then
	      q_aux <= D;
			else
			q_aux <= Q;
	  end if;
end if;
Q <= q_aux;
end process;


end Behavioral;

