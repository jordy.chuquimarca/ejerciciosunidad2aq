----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:01:46 07/11/2022 
-- Design Name: 
-- Module Name:    Eje17 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

entity Eje17 is
    Port ( clk,RESET,LOAD,ENP : in  STD_LOGIC;
           P : in  STD_LOGIC_VECTOR (3 downto 0);
           Q : inout  STD_LOGIC_VECTOR (3 downto 0));
end Eje17;

architecture Behavioral of Eje17 is
begin 
process (clk,RESET,LOAD,ENP)
begin
 if (RESET = '1') then
     Q <= "0000";
	 elsif(clk' event and clk = '1') then
	      if(LOAD = '0' and ENP = '-') then
			   Q <= P;
			elsif(LOAD = '1' and ENP = '0') then
			   Q <= Q;
			elsif(LOAD = '1' and ENP = '1') then
			   Q <= Q + 1;
			end if;
		end if;
	end process;
end Behavioral;

