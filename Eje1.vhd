----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:36:20 07/10/2022 
-- Design Name: 
-- Module Name:    Eje1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Eje1 is
    Port ( x,control : in  STD_LOGIC;
           z : out  STD_LOGIC);
end Eje1;

architecture Behavioral of Eje1 is

begin
z <= x when 
       (control = '1') ;
		 
end Behavioral;

