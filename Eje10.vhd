----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:56:55 07/10/2022 
-- Design Name: 
-- Module Name:    Eje10 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Eje10 is
    Port ( D : in STD_LOGIC_VECTOR(3 downto 0);
	        clk,CLR, : in  STD_LOGIC;
           Q,Qn : out  STD_LOGIC_VECTOR(3 downto 0));
end Eje10;

architecture Behavioral of Eje10 is
begin
process (clk,CLR)
begin
   if(clk' event and clk = '1') then
	  if(CLR = '1') then
	     Q <= D;
		  Qn <= not Q;
		else
		  Q <= "0000";
		  Qn <= "1111";
		 end if;
	end if;
end process;
end Behavioral;

