----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:45:54 07/10/2022 
-- Design Name: 
-- Module Name:    Eje3 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Eje3 is
    Port ( ffd,D,clk : in  STD_LOGIC;
           Q : out  STD_LOGIC);
end Eje3;

architecture Behavioral of Eje3 is
begin
process(clk)
begin
   if (clk'event and clk = '1') then
	    Q <= D;
	end if;
end process;
end Behavioral;

