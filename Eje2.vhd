----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:39:46 07/10/2022 
-- Design Name: 
-- Module Name:    Eje2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Eje2 is
    Port ( dato,control : in  STD_LOGIC;
           salida : out  STD_LOGIC);
end Eje2;

architecture Behavioral of Eje2 is
begin
process (dato,control)
begin
if control = '1' then 
   salida <= dato;
end if;
end process;


end Behavioral;

