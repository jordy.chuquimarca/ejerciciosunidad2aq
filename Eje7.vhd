----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:37:25 07/10/2022 
-- Design Name: 
-- Module Name:    Eje7 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Eje7 is
    Port ( d,clk : in  STD_LOGIC;
           q : out  STD_LOGIC);
end Eje7;

architecture Behavioral of Eje7 is
signal a,b: std_logic;
begin
process (clk)
begin
if (clk' event and clk = '1') then
    a <= d;
	 b <= a;
	 q <= d;
	end if;
end process;
end Behavioral;

