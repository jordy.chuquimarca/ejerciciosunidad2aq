----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:11:53 07/10/2022 
-- Design Name: 
-- Module Name:    Eje4 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Eje4 is
    Port ( D,clk,RESET : in  STD_LOGIC;
           Q : out  STD_LOGIC);
end Eje4;

architecture Behavioral of Eje4 is
begin
  process(clk,D,RESET)
begin
   if RESET = '1' then
	   Q <= '0';
	elsif(clk' event and clk = '1') then
      Q <= D;
  end if;
end process;  


end Behavioral;

