----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:56:57 07/11/2022 
-- Design Name: 
-- Module Name:    Eje16 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;

--use UNISIM.VComponents.all;

entity Eje16 is
    Port ( clk,LDA,UP : in  STD_LOGIC;
           D : in  STD_LOGIC_VECTOR (2 downto 0);
           Q : inout  STD_LOGIC_VECTOR (2 downto 0));
end Eje16;

architecture Behavioral of Eje16 is

begin
process (clk,LDA,D,UP)
begin
if(clk' event and clk = '1')then
  if (LDA = '0')then
      Q <= D;
	elsif(UP = '1') then
	   Q <= Q + 1;
	else 
	    Q <= Q - 1;
	end if;
 end if;
 end process;

end Behavioral;

